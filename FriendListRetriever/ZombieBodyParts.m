//
//  ZombieBodyParts.m
//  ZombieBooth
//
//  Created by user on 2/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import "ZombieBodyParts.h"

@interface ZombieBodyParts() <UICollectionViewDataSource, UICollectionViewDelegate>


@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation ZombieBodyParts

NSString * _cellIdentifier = @"CollectionViewCell";
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) awakeFromNib
{
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:_cellIdentifier];
    if (self.zombiePartsArray == nil) {
        self.zombiePartsArray = [[NSMutableArray alloc] init];
    }
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < 66; i++) {
            NSString *imageString = [NSString stringWithFormat:@"pic%d_group%d.jpg", i, j];
            UIImage *image = [UIImage imageNamed:imageString];
            
            if (image == nil) {
                continue;
            }
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            
            [imageView setAccessibilityLabel:imageString];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            imageView.frame = CGRectMake(0, 0, 75, 75);
            imageView.tag = i;
            [self.zombiePartsArray addObject:imageView];
        }
    }
    
}

- (IBAction)hideView:(id)sender {
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[self setAlpha:0.0];
//    [UIView setAnimationDidStopSelector:@selector(someMethod)];
	[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self cache:YES];
	[UIView commitAnimations];
//    [UIView animateWithDuration:0.25 delay:0.15 options: UIViewAnimationOptionTransitionCurlUp animations:^{ self.frame =  CGRectMake(0, 0, 0, 0); } completion:^(BOOL finished){ [self removeFromSuperview]; }];
    
}

-(void) loadGroupPictures:(NSString *)groupName
{
    if (self.zombiePartsArray == nil) {
        self.zombiePartsArray = [[NSMutableArray alloc] init];
    } else {
        [self.zombiePartsArray removeAllObjects];
    }
    
    for (int i = 0; i < 16; i++) {
        NSString *imageString = [NSString stringWithFormat:@"pic%d_%@.jpg", i, groupName];
        UIImage *image = [UIImage imageNamed:imageString];
        if (image == nil) {
            continue;
        }
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageView setAccessibilityLabel:imageString];
        imageView.frame = CGRectMake(0, 0, 75, 75);
        imageView.tag = i;
        [self.zombiePartsArray addObject:imageView];
    }
    [self.collectionView reloadData];
}

- (IBAction)loadImageFromGroup:(id)sender {
    [self.zombiePartsArray removeAllObjects];
    UIButton *btn = (UIButton *)sender;
    for (int i = 0; i < 16; i++) {
        NSString *imageString = [NSString stringWithFormat:@"pic%d_group%ld.jpg", i, (long)btn.tag];
        UIImage *image = [UIImage imageNamed:imageString];
        if (image == nil) {
            continue;
        }
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageView setAccessibilityLabel:imageString];
        imageView.frame = CGRectMake(0, 0, 75, 75);
        imageView.tag = i;
        [self.zombiePartsArray addObject:imageView];
    }
    [self.collectionView reloadData];
}


-(void) someMethod
{
    [self removeFromSuperview];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.zombiePartsArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_cellIdentifier forIndexPath:indexPath];
    [cell addSubview: self.zombiePartsArray[indexPath.row]];
    cell.backgroundColor = [UIColor blueColor];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate != nil) {
        [self.delegate didSelectImageInCollectionView:[self.zombiePartsArray objectAtIndex:indexPath.row]];
        [self removeFromSuperview];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
