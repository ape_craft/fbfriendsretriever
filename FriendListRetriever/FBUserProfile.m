//
//  FBUserProfile.m
//  FriendListRetriever
//
//  Created by user on 2/17/14.
//  Copyright (c) 2014 hirerussians. All rights reserved.
//

#import "FBUserProfile.h"

@implementation FBUserProfile

-(id) initWithFBGraphUser: (NSDictionary<FBGraphUser> *) user
{
    self = [super init];
    self.name = user.name;
    self.user_id = user.id;
    self.imageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&width=400&height=400", user.id];
    return self;
}

@end
