//
//  MainViewController.m
//  FriendListRetriever
//
//  Created by user on 2/17/14.
//  Copyright (c) 2014 hirerussians. All rights reserved.
//

#import "MainViewController.h"
#import "FBUserProfile.h"
#import "ProfilePictureDownloader.h"
#import "ZombieBoothViewController.h"

@interface MainViewController () <FBLoginViewDelegate, FBFriendPickerDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *_friendListArray;
    IBOutlet UITableView *_friendsListTable;
}
@property (strong, nonatomic) NSMutableDictionary *profileImageDownloadsInProgress;
@end

@implementation MainViewController

@synthesize friendListTable = _friendsListTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    FBLoginView *fbLoginView = [[FBLoginView alloc] init];
    fbLoginView.readPermissions = @[@"basic_info", @"email", @"user_likes", @"friends_birthday", @"user_birthday"];
    fbLoginView.frame = CGRectMake(96, 91, 184, 53);
    [fbLoginView sizeToFit];
    fbLoginView.delegate = self;
    [self.view addSubview:fbLoginView];
    
//    _friendListArray = [[NSMutableArray alloc] init];
    self.friends = [[NSMutableArray alloc] init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark FBLoginViewDelegate
-(void) loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
//    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
//    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
//                                                  NSDictionary* result,
//                                                  NSError *error) {
//        NSArray *friends = [result objectForKey:@"data"];
//        _friendListArray = [[NSMutableArray alloc] init];
//        //        NSLog(@"Found: %i friends", friends.count);
//        for (NSDictionary<FBGraphUser>* friend in friends) {
//            [_friendListArray addObject:[[FBUserProfile alloc] init]];
//        }
//        [_friendsListTable reloadData];
//
//    }];
}

-(void) loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome to Facebook"
//                                                    message:[NSString stringWithFormat:@"You logged in as: %@", [user name]]
//                                                   delegate:self cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil, nil];
//    [alert show];
    
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    [self.friends removeAllObjects];
    [_friendsListTable reloadData];
}

#pragma mark - UITableViewDelegate


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.friends count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    FBUserProfile *user = [self.friends objectAtIndex:indexPath.row];
    if(!user.image) {
//        if (tableView.dragging == NO && tableView.decelerating == NO) {
            [self startProfilePictureDownload:user forIndexPath:indexPath];
//        } else {
            cell.imageView.image = [UIImage imageNamed:@"fbLogo.png"];
            cell.textLabel.text = user.name;
            cell.detailTextLabel.text = user.user_id;
//        }
        
    } else {
        cell.textLabel.text = user.name;
        cell.detailTextLabel.text = user.user_id;
        cell.imageView.image = user.image;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FBUserProfile *user = [self.friends objectAtIndex:indexPath.row];
    ZombieBoothViewController* viewController = [[ZombieBoothViewController alloc] initWithImage:user.originalImage];
    [self.navigationController pushViewController:viewController animated:YES];
    
}


#pragma mark - Table Cell image support

-(void) startProfilePictureDownload: (FBUserProfile *)user forIndexPath: (NSIndexPath *)indexPath
{
    ProfilePictureDownloader *pictureDownloader = [self.profileImageDownloadsInProgress objectForKey:indexPath];
    if (pictureDownloader == nil) {
        pictureDownloader = [[ProfilePictureDownloader alloc] init];
        pictureDownloader.userProfile = user;
        [pictureDownloader setCompletionHandler:^{
            UITableViewCell *cell = [_friendsListTable cellForRowAtIndexPath:indexPath];
            cell.imageView.image = user.image;
//            cell.textLabel.text = user.name;
//            cell.detailTextLabel.text = user.user_id;
            [self.profileImageDownloadsInProgress removeObjectForKey:indexPath];
        }];
        [self.profileImageDownloadsInProgress setObject:pictureDownloader forKey:indexPath];
        [pictureDownloader startDownload];
    }
}

- (void)dealloc {

//    [super dealloc];
}
@end
