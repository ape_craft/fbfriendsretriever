//
//  ZombieWorker.m
//  ZombieBooth
//
//  Created by user on 1/31/14.
//  Copyright (c) 2014 tz. All rights reserved.
//
#import <AssetsLibrary/AssetsLibrary.h>
#import "ZombieWorker.h"
#import "ZombieBoothViewController.h"

@interface ZombieWorker () <UIGestureRecognizerDelegate> {
    UIImage *_targetImage;
    UIImage *_chosenImage;
    NSMutableArray *_droppedImages;
    ALAsset *_imageToSave;
    ImageState _imageState;
    id _delegate;
}

@end

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};
CGFloat RadiansToDegrees(CGFloat radians) {return radians * 180/M_PI;};

@implementation ZombieWorker
@synthesize imageState = _imageState;

-(id) initWithImage: (UIImage *) image
{
    self = [super init];
    _targetImage = image;
    _imageState = ImageStateEditing;
    return self;
}

-(UIImage *) getImage
{
    return _targetImage;
}

-(void) setChooseImage: (UIImage *) image
{
    _chosenImage = image;
}

-(void) modifyImage:(UIImage *)image  Rotate: (CGFloat) rotate Scale: (CGFloat) scale
{
    
}

- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize
{
    _imageState = ImageStateEditing;
    UIImage *newImage = nil;
    
    CGSize imageSize = _chosenImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    
    // this is actually the interesting part:
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [_chosenImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(newImage == nil) NSLog(@"could not scale image");
    
    
    return newImage ;
}

- (UIImage *)imageRotatedByRadians:(CGFloat)radians
{
    _imageState = ImageStateEditing;
    return [self imageRotatedByDegrees:RadiansToDegrees(radians)];
}

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees
{
    _imageState = ImageStateEditing;
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, _chosenImage.size.width, _chosenImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
//    [rotatedViewBox release];
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-_chosenImage.size.width / 2, -_chosenImage.size.height / 2, _chosenImage.size.width, _chosenImage.size.height), [_chosenImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

- (UIImage *) position: (CGPoint) position
{
    _imageState = ImageStateOverlayed;
    // begin a graphics context of sufficient size
	UIGraphicsBeginImageContext(_targetImage.size);
    
	// draw original image into the context
	[_targetImage drawAtPoint:CGPointZero];
    
	// get the context for CoreGraphics
	CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Transform image
    CGContextTranslateCTM(ctx, position.x, position.y);
    
	// make circle rect 5 px from border
	CGRect circleRect = CGRectMake(position.x, position.y, _chosenImage.size.width, _chosenImage.size.height);
	circleRect = CGRectInset(circleRect, 5, 5);
    
	// draw image
    CGContextDrawImage(ctx, circleRect, _chosenImage.CGImage);
    
//    UIImage *img = [UIImage imageWithCIImage:_targetImage.CIImage];
//    [img drawInRect:imageRect];
	// make image out of bitmap context
	_targetImage = UIGraphicsGetImageFromCurrentImageContext();
    
	// free the context
	UIGraphicsEndImageContext();
    
	return _targetImage;
}

- (UIImage *)getViewShot: (UIView *) view
{
    _imageState = ImageStateOverlayed;
    _targetImage = [self imageWithView:view andOpaqueFlag:YES];
    return _targetImage;
}

- (UIImage *) imageWithView:(UIView *)view andOpaqueFlag:(BOOL)isOpaque
{
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, isOpaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

@end
