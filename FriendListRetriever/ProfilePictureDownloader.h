//
//  ProfilePictureDownloader.h
//  FriendListRetriever
//
//  Created by user on 2/17/14.
//  Copyright (c) 2014 hirerussians. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FBUserProfile.h"

@interface ProfilePictureDownloader : NSObject

@property (strong, nonatomic) FBUserProfile *userProfile;
@property (strong, nonatomic) void (^completionHandler)(void);

-(void) startDownload;
-(void) cancelDownload;

@end
