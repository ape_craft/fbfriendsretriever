//
//  ZombieWorker.h
//  ZombieBooth
//
//  Created by user on 1/31/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    ImageStateWaiting,
    ImageStatePhotoLoaded,
    ImageStateEditing,
    ImageStateOverlayed,
    ImageStatePhotoSaved
} ImageState;

@interface ZombieWorker : NSObject

@property (readonly, nonatomic) ImageState imageState;

- (id)initWithImage: (UIImage *) image;
- (void)modifyImage: (UIImage *) image Rotate: (CGFloat) rotate Scale: (CGFloat) scale;
- (void)setChooseImage: (UIImage *) image;
- (UIImage *)getImage;
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
- (UIImage *)imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)position: (CGPoint) position;
- (UIImage *)getViewShot: (UIView *) view;

@end
