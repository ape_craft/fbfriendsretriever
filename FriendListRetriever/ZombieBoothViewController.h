//
//  ZombieBoothViewController.h
//  ZombieBooth
//
//  Created by user on 1/31/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

typedef enum
{
    MoveUIViewNothing,
    MoveUIViewChoosenImage,
    MoveUIViewWorkspaceView,
    MoveUIViewTextLabel
} MoveUIView;

@interface ZombieBoothViewController : UIViewController

-(id) initWithImage: (UIImage *) image;

@end
