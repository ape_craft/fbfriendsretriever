//
//  ZombieBodyParts.h
//  ZombieBooth
//
//  Created by user on 2/20/14.
//  Copyright (c) 2014 tz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZombieBodyPartsDelegate <NSObject>

-(void) didSelectImageInCollectionView: (UIImageView *) selectedImageView;

@end

@interface ZombieBodyParts : UIView

-(void) loadGroupPictures: (NSString*) groupName;

@property (strong, nonatomic) id<ZombieBodyPartsDelegate> delegate;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSMutableArray *zombiePartsArray;

@end
