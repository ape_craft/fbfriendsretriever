//
//  MainViewController.h
//  FriendListRetriever
//
//  Created by user on 2/17/14.
//  Copyright (c) 2014 hirerussians. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface MainViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *friends;
@property (strong, nonatomic) UITableView *friendListTable;

@end
