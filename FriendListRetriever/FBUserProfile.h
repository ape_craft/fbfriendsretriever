//
//  FBUserProfile.h
//  FriendListRetriever
//
//  Created by user on 2/17/14.
//  Copyright (c) 2014 hirerussians. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface FBUserProfile : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImage *originalImage;
@property (strong, nonatomic) NSString *imageURL;

-(id) initWithFBGraphUser: (NSDictionary<FBGraphUser> *) user;

@end
