//
//  ZombieBoothViewController.m
//  ZombieBooth
//
//  Created by user on 1/31/14.
//  Copyright (c) 2014 tz. All rights reserved.
//
#import <FacebookSDK/FacebookSDK.h>

#import "ZombieBoothViewController.h"
#import "ZombieWorker.h"
#import "ZombieBodyParts.h"

@interface ZombieBoothViewController () <UIGestureRecognizerDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, ZombieBodyPartsDelegate>{
    NSMutableArray *_photoPiecesArray;
    
    UIPinchGestureRecognizer *_pinchGesture;
    UIPanGestureRecognizer *_panGesture;
    UILongPressGestureRecognizer *_longPressGesture;
    UIRotationGestureRecognizer *_rotationGesture;
    
    ZombieWorker *_zombieWorker;

    UIImageView *_chosenImageView;
    UIImageView *_demonstrateImage;
    UIView *_workplaceView;
    
    MoveUIView _uiViewToMove;
    CGPoint _coordsBeforeMove;
}

@property (strong, nonatomic) NSString *objectID;
@property (strong, nonatomic) UIView *pieceForReset;

@property (strong ,nonatomic) IBOutlet UIView *selectionView;
@property (strong, nonatomic) IBOutlet UIView *defaultScreen;
@property (strong, nonatomic) IBOutlet UIView *editingView;
@property (strong, nonatomic) IBOutlet UIView *categoryChooseView;

@end

@implementation ZombieBoothViewController

//@synthesize collectionView = _skillsCollectionView;

static NSString* _cellIdentifier = @"Cell";

#pragma mark - Initialize View

-(id) initWithImage:(UIImage *)image
{
    self = [super init];
    _zombieWorker = [[ZombieWorker alloc] initWithImage:image];
    
    _workplaceView = [[UIImageView alloc] initWithImage:image];
    [_workplaceView setUserInteractionEnabled:YES];
    [_workplaceView setClipsToBounds:YES];
    
    _workplaceView.contentMode = UIViewContentModeScaleAspectFit;
    
    _workplaceView.frame = CGRectMake(20, 121, 280, 280.0 / (image.size.width / image.size.height));
    
    _photoPiecesArray = [[NSMutableArray alloc] init];
    _pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
    _pinchGesture.delegate = self;
    
    _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panPiece:)];
    _panGesture.delegate = self;
    
    _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(showResetMenu:)];
    _longPressGesture.delegate = self;
    
    _rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotatePiece:)];
    _rotationGesture.delegate = self;
    
    _chosenImageView = [[UIImageView alloc] init];
    [_chosenImageView setClipsToBounds:YES];
    
    
    for (int i = 0; i < 16; i++) {
        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"pic%d.jpg", i]];
        imgView.tag = i;
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        
        imgView.frame = CGRectMake(0, 0, 75, 75);
        [_photoPiecesArray addObject:imgView];
        
        NSLog(@"%@", [NSString stringWithFormat:@"pic%d", i]);
        NSLog(@"%@", imgView.image);
    }
    
    [self.view addSubview:_workplaceView];
    [self.view addGestureRecognizer:_pinchGesture];
    [self.view addGestureRecognizer:_panGesture];
    [self.view addGestureRecognizer:_longPressGesture];
    [self.view addGestureRecognizer:_rotationGesture];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //        [self.workplaceView setClipsToBounds:YES];
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.editingView setHidden:YES];
    [self.view addSubview: self.defaultScreen];
    _uiViewToMove = MoveUIViewNothing;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ImagePicker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    if (_zombieWorker.imageState == ImageStateWaiting) {
        return;
    }
    
    for (UIView *view in _chosenImageView.subviews) {
        [view removeFromSuperview];
    }
    for (UIView *view in _workplaceView.subviews) {
        [view removeFromSuperview];
    }
    
    UIImage *image = chosenImage;
    
    [_zombieWorker setChooseImage:image];
    
    _chosenImageView = [[UIImageView alloc] init];
    _chosenImageView.image = image;
    [_chosenImageView setUserInteractionEnabled:YES];
    
    _chosenImageView.contentMode = UIViewContentModeScaleAspectFit;
    _chosenImageView.frame = CGRectMake(_workplaceView.frame.size.width / 2 - 60,
                                        _workplaceView.frame.size.height / 2 - 60, 121, 121);
    [_workplaceView addSubview:_chosenImageView];
    [self.editingView setHidden:NO];
    [self.selectionView setHidden:YES];
//    _zombieWorker = [[ZombieWorker alloc] initWithImage:chosenImage];
//    
//    _workplaceView = [[UIImageView alloc] initWithImage:chosenImage];
//    [_workplaceView setUserInteractionEnabled:YES];
//    [_workplaceView setClipsToBounds:YES];
//    
//    _workplaceView.contentMode = UIViewContentModeScaleAspectFit;
//    
//    _workplaceView.frame = CGRectMake(20, 104, 280, 280.0 / (chosenImage.size.width / chosenImage.size.height));
//    
//    _photoPiecesArray = [[NSMutableArray alloc] init];
//    _pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
//    _pinchGesture.delegate = self;
//    
//    _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panPiece:)];
//    _panGesture.delegate = self;
//    
//    _longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(showResetMenu:)];
//    _longPressGesture.delegate = self;
//    
//    _rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotatePiece:)];
//    _rotationGesture.delegate = self;
//    
//    _chosenImageView = [[UIImageView alloc] init];
//    [_chosenImageView setClipsToBounds:YES];
//
//    
//    for (int i = 0; i < 16; i++) {
//        UIImageView *imgView = [[UIImageView alloc] init];
//        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"pic%d.jpg", i]];
//        imgView.tag = i;
//        imgView.contentMode = UIViewContentModeScaleAspectFit;
//        
//        imgView.frame = CGRectMake(0, 0, 75, 75);
//        [_photoPiecesArray addObject:imgView];
//        
//        NSLog(@"%@", [NSString stringWithFormat:@"pic%d", i]);
//        NSLog(@"%@", imgView.image);
//    }
//
//    [self.view addSubview:_workplaceView];
//    [self.view addGestureRecognizer:_pinchGesture];
//    [self.view addGestureRecognizer:_panGesture];
//    [self.view addGestureRecognizer:_longPressGesture];
//    [self.view addGestureRecognizer:_rotationGesture];
//    
//    [self.defaultScreen removeFromSuperview];
//    [self.view addSubview:_workplaceView];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Button actions

- (IBAction)selectPhoto:(id)sender {
    _uiViewToMove = MoveUIViewChoosenImage;
    _coordsBeforeMove = _workplaceView.frame.origin;
    [self.categoryChooseView setHidden:YES];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)combineWithOriginal:(id)sender {
    
    if (_uiViewToMove == MoveUIViewWorkspaceView) {
        [self.editingView setHidden:YES];
        [self.selectionView setHidden:NO];
        return;
    }
    if (_zombieWorker.imageState == ImageStateWaiting) {
        return;
    }
    [_zombieWorker getViewShot:_workplaceView];
    
    [_chosenImageView removeFromSuperview];
    [_workplaceView removeFromSuperview];
    
    _workplaceView = [[UIImageView alloc] initWithImage:[_zombieWorker getImage]];
    [_workplaceView setClipsToBounds:YES];
    [_workplaceView setUserInteractionEnabled:YES];
    _workplaceView.contentMode = UIViewContentModeScaleAspectFit;
    _workplaceView.frame = CGRectMake(_coordsBeforeMove.x, _coordsBeforeMove.y, _workplaceView.frame.size.width, _workplaceView.frame.size.height);
    
    [self.view addSubview:_workplaceView];
    [self.editingView setHidden:YES];
    [self.selectionView setHidden:NO];
}

- (IBAction)shareButtonClicked:(id)sender {
    if (_zombieWorker.imageState == ImageStateWaiting) {
        return;
    }
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Sharing option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Share on Facebook",
                            @"Share on Twitter",
                            @"Share on VKontakte",
                            @"Save to Camera Roll",
                            @"Rate this App",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)saveImage:(id)sender {
    if (_zombieWorker.imageState == ImageStateWaiting) {
        return;
    }
    if (_zombieWorker.imageState == ImageStateEditing) {
        [self combineWithOriginal:sender];
    }
    UIImageWriteToSavedPhotosAlbum([_zombieWorker getImage],nil,nil,nil);
}
- (IBAction)showSelectionView:(id)sender {
    [self.categoryChooseView setHidden: NO];
//    [self.categoryChooseView setFrame:CGRectMake(0.0, 600, 67, 0.0)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    
    [self.categoryChooseView setFrame:CGRectMake(self.view.window.bounds.origin.x, 293, 67, 202)];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.categoryChooseView cache:YES];
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview: self.categoryChooseView];
    
    [UIView commitAnimations];

}

- (IBAction)openCategoryView:(id)sender {
    
    _uiViewToMove = MoveUIViewChoosenImage;
    _coordsBeforeMove = _workplaceView.frame.origin;
    [self.categoryChooseView setHidden:YES];
    ZombieBodyParts *bodyPartsView = [[[NSBundle mainBundle] loadNibNamed:@"ZombieBodyParts" owner:self options:nil] objectAtIndex:0];
    UIButton *btn = (UIButton *)sender;
    [bodyPartsView loadGroupPictures:btn.accessibilityLabel];
    
    bodyPartsView.delegate = self;
    
    [bodyPartsView setFrame:CGRectMake(0, 430, self.view.window.bounds.size.width, self.view.window.bounds.size.height - 72)];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    
    [bodyPartsView setFrame:CGRectMake(self.view.window.bounds.origin.x, self.view.window.bounds.origin.y, self.view.window.bounds.size.width, self.view.window.bounds.size.height - 72)];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:bodyPartsView cache:YES];
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview: bodyPartsView];
    
    [UIView commitAnimations];
}

- (IBAction)cancelEdition:(id)sender {
    switch (_uiViewToMove) {
        case MoveUIViewChoosenImage:
            [_chosenImageView removeFromSuperview];
            _chosenImageView = nil;
            break;
        case MoveUIViewWorkspaceView:
            _workplaceView.frame = CGRectMake(_coordsBeforeMove.x, _coordsBeforeMove.y, _workplaceView.frame.size.width, _workplaceView.frame.size.height);
            break;
        default:
            break;
    }
    [self.selectionView setHidden:NO];
    [self.editingView setHidden:YES];
}
- (IBAction)moveWorkspaceView:(id)sender {
    _coordsBeforeMove = [_workplaceView frame].origin;
    [self.editingView setHidden:NO];
    [self.selectionView setHidden:YES];
    _uiViewToMove = MoveUIViewWorkspaceView;
}


#pragma mark - ZombieBodyPartsDelegate
-(void) didSelectImageInCollectionView:(UIImageView *)selectedImageView
{
    if (_zombieWorker.imageState == ImageStateWaiting) {
        return;
    }
    
    for (UIView *view in _chosenImageView.subviews) {
        [view removeFromSuperview];
    }
    for (UIView *view in _workplaceView.subviews) {
        [view removeFromSuperview];
    }
    
    UIImage *image = [UIImage imageNamed:selectedImageView.accessibilityLabel];
    
    [_zombieWorker setChooseImage:image];
    
    _chosenImageView = [[UIImageView alloc] init];
    _chosenImageView.image = image;
    [_chosenImageView setUserInteractionEnabled:YES];
    
    _chosenImageView.contentMode = UIViewContentModeScaleAspectFit;
    _chosenImageView.frame = CGRectMake(_workplaceView.frame.size.width / 2 - 60,
                                        _workplaceView.frame.size.height / 2 - 60, 121, 121);
    [_workplaceView addSubview:_chosenImageView];
    [self.editingView setHidden:NO];
    [self.selectionView setHidden:YES];
}

#pragma mark - ActionSheet delegate

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
        {
//            [[SPSocialPoster sharedPoster] facebookPostImage:[_zombieWorker getImage] withStatus:@"hzmz" viewForAlerts:self.view completionBlock:^(NSError *completion){
//                
//            }];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
                                                            message:@"Currently you cannot share your pic."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case 1:
        {
            
//            [[SPSocialPoster sharedPoster] twitterPostImage:[_zombieWorker getImage] withStatus:@"hzmz" viewForAlerts:self.view completionBlock:^(NSError *completion){
//                if ([[completion localizedDescription] length] > 0) {
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
//                                                                    message:[completion localizedDescription]
//                                                                   delegate:self
//                                                          cancelButtonTitle:@"OK"
//                                                          otherButtonTitles:nil, nil];
//                    [alert show];
//                } else {
//                    [SPUIUtils popLockAndDarkenScreen];
//                    [SPUIUtils popInterfaceLock];
//                    [SPUIUtils popDarkenScreen];
//                }
//            }];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
                                                            message:@"Currently you cannot share your pic."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case 2:
        {
//            [[SPSocialPoster sharedPoster] vkontaktePostImage:[_zombieWorker getImage] withStatus:@"hzmz" viewForAlerts:self.view completionBlock:^(NSError *completion){
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
//                                                            message:[completion localizedDescription]
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil, nil];
//            [alert show];
//        }];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
                                                            message:@"Currently you cannot share your pic."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case 3:
            UIImageWriteToSavedPhotosAlbum([_zombieWorker getImage],nil,nil,nil);
            break;
        default:
            break;
    }
}

#pragma mark - UIGestureRecognizers methods

/**
 Scale and rotation transforms are applied relative to the layer's anchor point this method moves a gesture recognizer's view's anchor point between the user's fingers.
 */

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
//        UIView *piece = _chosenImageView;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
//        piece.layer.anchorPoint = _chosenImageView.center;
        piece.center = locationInSuperview;
    }
}

/**
 Scale the piece by the current scale.
 Reset the gesture recognizer's rotation to 0 after applying so the next callback is a delta from the current scale.
 */

- (IBAction)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer
{
    CGFloat scale = _pinchGesture.scale;
    _chosenImageView.transform = CGAffineTransformScale(_chosenImageView.transform, scale, scale);
    _pinchGesture.scale = 1.0;
}

/**
 Display a menu with a single item to allow the piece's transform to be reset.
 */

- (IBAction)showResetMenu:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        
        [self becomeFirstResponder];
        self.pieceForReset = [gestureRecognizer view];
        
        /*
         Set up the reset menu.
         */
        NSString *menuItemTitle = NSLocalizedString(@"Reset", @"Reset menu item title");
        UIMenuItem *resetMenuItem = [[UIMenuItem alloc] initWithTitle:menuItemTitle action:@selector(resetPiece:)];
        
        NSString *removeMenuItemTitle = NSLocalizedString(@"Remove", @"Remove menu item title");
        UIMenuItem *removeMenuItem = [[UIMenuItem alloc] initWithTitle:removeMenuItemTitle action:@selector(removePiece:)];
        
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setMenuItems:@[resetMenuItem, removeMenuItem]];
        
        CGPoint location = [gestureRecognizer locationInView:[gestureRecognizer view]];
        CGRect menuLocation = CGRectMake(location.x, location.y, 0, 0);
        [menuController setTargetRect:menuLocation inView:[gestureRecognizer view]];
        
        [menuController setMenuVisible:YES animated:YES];
    }
}

/**
 Animate back to the default anchor point and transform.
 */

- (void)resetPiece:(UIMenuController *)controller
{
    UIView *pieceForReset = self.pieceForReset;
    
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(pieceForReset.bounds), CGRectGetMidY(pieceForReset.bounds));
    CGPoint locationInSuperview = [pieceForReset convertPoint:centerPoint toView:[pieceForReset superview]];
    
    [[pieceForReset layer] setAnchorPoint:CGPointMake(0.5, 0.5)];
    [pieceForReset setCenter:locationInSuperview];
    
    [UIView beginAnimations:nil context:nil];
    [pieceForReset setTransform:CGAffineTransformIdentity];
    [UIView commitAnimations];
}

- (void)removePiece:(UIMenuController *)controller
{
    [_chosenImageView removeFromSuperview];
    _chosenImageView = nil;
}

/**
 Shift the piece's center by the pan amount.
 Reset the gesture recognizer's translation to {0, 0} after applying so the next callback is a delta from the current position.
 */

- (IBAction)panPiece:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGPoint translation = [_panGesture translationInView:self.view];
    CGPoint imageViewPosition = _chosenImageView.center;
    
    switch (_uiViewToMove) {
        case MoveUIViewChoosenImage:
            imageViewPosition = _chosenImageView.center;
            imageViewPosition.x += translation.x;
            imageViewPosition.y += translation.y;
            _chosenImageView.center = imageViewPosition;
            break;
        case MoveUIViewWorkspaceView:
            imageViewPosition = _workplaceView.center;
            imageViewPosition.x += translation.x;
            imageViewPosition.y += translation.y;
            _workplaceView.center = imageViewPosition;
            break;
        default:
            break;
    }
    [_panGesture setTranslation:CGPointZero inView:self.view];
}

/**
 Rotate the piece by the current rotation.
 Reset the gesture recognizer's rotation to 0 after applying so the next callback is a delta from the current rotation.
 */

- (IBAction)rotatePiece:(UIRotationGestureRecognizer *)gestureRecognizer
{
    CGFloat angle = _rotationGesture.rotation;
    _chosenImageView.transform = CGAffineTransformRotate(_chosenImageView.transform, angle);
    _rotationGesture.rotation = 0.0;
}

/**
 Ensure that the pinch, pan and rotate gesture recognizers on a particular view can all recognize simultaneously.
 Prevent other gesture recognizers from recognizing simultaneously.
 */

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self.categoryChooseView isHidden] == NO) {
        [self.categoryChooseView setHidden:YES];
    }
}

// UIMenuController requires that we can become first responder or it won't display
- (BOOL)canBecomeFirstResponder
{
    return YES;
}

@end
